# uw_reports

Think from the perspective of an Underwriter and identify potential factors that may result in an auto policy being flagged for non-renewal. Develop a machine learning model to predict non-renewal.

